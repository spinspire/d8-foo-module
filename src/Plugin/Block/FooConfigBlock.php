<?php

/**
 * @file
 * Contains \Drupal\foo\Plugin\Block\FooBlock.
 */

namespace Drupal\foo\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FooBlock' block.
 *
 * @Block(
 *  id = "foo_config_block",
 *  admin_label = @Translation("Foo Config Block"),
 * )
 */
class FooConfigBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\foo\Form\FooConfigForm');
  }
}
