<?php

/**
 * @file
 * Contains \Drupal\foo\Plugin\Block\FooBlock.
 */

namespace Drupal\foo\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FooBlock' block.
 *
 * @Block(
 *  id = "foo_form_block",
 *  admin_label = @Translation("Foo Form Block"),
 * )
 */
class FooFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\foo\Form\FooForm');
  }
}
