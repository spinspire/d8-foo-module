<?php

/**
 * @file
 * Contains \Drupal\foo\Plugin\Block\FooBlock.
 */

namespace Drupal\foo\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FooBlock' block.
 *
 * @Block(
 *  id = "foo_block",
 *  admin_label = @Translation("Basic Foo Block"),
 * )
 */
class FooBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['foo_block']['#markup'] = 'Implement FooBlock.';
    return $build;
  }
}
