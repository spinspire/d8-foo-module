<?php
# Assign namespace for controller.
# The convention is to use Drupal\(modulename)\...
namespace Drupal\foo\Controller;

# Include these dependencies.
use Drupal\Core\Controller\ControllerBase;

# Our controller extends the ControllerBase class.
class FooControllers extends ControllerBase {
  public function page1() {
    # Render arrays are generally what is returned by druapl functions.
    return ['#markup' => $this->t('This is page 1. And then some.')];
  }

  public function page2($param1, $param2) {
    return ['#markup' => $this->t('This is page 2 - with params %param1 and %param2.', ['%param1' => $param1, '%param2' => $param2])];
  }
}
