<?php

/**
 * @file
 * Contains Drupal\foo\Form\FooConfigForm.
 */

namespace Drupal\foo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FooConfigForm.
 *
 * @package Drupal\foo\Form
 */
class FooConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'foo.fooconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'foo_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('foo.fooconfig');
    $form['default_greeting'] = array(
      '#type' => 'select',
      '#title' => $this->t('Your preferred greeting'),
      '#description' => $this->t('Pick our preferred greeting.'),
      '#options' => array(
        'Hello %name!' => $this->t('Hello'),
        'Hi %name!' => $this->t('Hi'),
        'Howdy %name!' => $this->t('Howdy')
      ),
      '#required' => TRUE,
      '#default_value' => $config->get('default_greeting'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('foo.fooconfig')
      ->set('default_greeting', $form_state->getValue('default_greeting'))
      ->save();
  }

}
