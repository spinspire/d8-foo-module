<?php

/**
 * @file
 * Contains \Drupal\foo\Form\FooForm.
 */

namespace Drupal\foo\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FooForm.
 *
 * @package Drupal\foo\Form
 */
class FooForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'foo_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('foo.fooconfig');
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your Name'),
      '#description' => $this->t('Enter your name.'),
      '#maxlength' => 32,
      '#size' => 32,
      '#required' => TRUE,
    );

    $form['greeting'] = array(
      '#type' => 'select',
      '#title' => $this->t('Your preferred greeting'),
      '#description' => $this->t('Pick our preferred greeting.'),
      '#options' => array(
        'Hello %name!' => $this->t('Hello'),
        'Hi %name!' => $this->t('Hi'),
        'Howdy %name!' => $this->t('Howdy')
      ),
      '#default_value' => $config->get('default_greeting'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Greet'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $greeting = $form_state->getValue('greeting');
    $name = $form_state->getValue('name');
    drupal_set_message(t($greeting, array('%name' => $name)));
  }
}
